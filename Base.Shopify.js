const Shopify = new (function() {
	/*
	* initialise: Setup our needed delegates
	*/
	this.initialise = () => {

		const scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
		if (window.ShopifyBuy) {
			if (window.ShopifyBuy.UI) {
				ShopifyBuyInit();
			} else {
				loadScript();
			}
		} else {
			loadScript();
		}

		function loadScript() {
			const script = document.createElement('script');
			script.async = true;
			script.src = scriptURL;
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
			script.onload = ShopifyBuyInit;
		}


		function ShopifyBuyInit() {
			const client = ShopifyBuy.buildClient({
				domain: '#',
				storefrontAccessToken: '#',
				appId: '#',
			});

			ShopifyBuy.UI.onReady(client).then(ui => {

				$(".shopifyBtn").each(function() {
					const elementId = $(this).attr("id");
					const dataId = $(this).attr("data-id");

					console.log(elementId);
					console.log(dataId);

					ui.createComponent('product', {
						id: [parseInt(dataId)],
						node: document.getElementById(elementId),
						moneyFormat: '%24%7B%7Bamount%7D%7D',
						options: {
							"product": {
								"layout": "horizontal",
								"variantId": "all",
								"width": "100%",
								"contents": {
									"img": false,
									"imgWithCarousel": true,
									"variantTitle": false,
									"description": true,
									"buttonWithQuantity": true,
									"button": false,
									"quantity": false
								},
								"text": {
									"button": "Add to Cart "
								},
								"styles": {
									"product": {
										"text-align": "left",
										"@media (min-width: 601px)": {
											"max-width": "100%",
											"margin-left": "0",
											"margin-bottom": "50px"
										}
									},
									"button": {
										"background-color": "#7b9c7b",
										"font-family": "Karla, sans-serif",
										"font-size": "14px",
										"padding-top": "15px",
										"padding-bottom": "15px",
										":hover": {
											"background-color": "#6f8c6f"
										},
										"font-weight": "normal",
										":focus": {
											"background-color": "#6f8c6f"
										}
									},
									"variantTitle": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"title": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal",
										"font-size": "26px"
									},
									"description": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"price": {
										"font-family": "Karla, sans-serif",
										"font-size": "18px",
										"color": "#7b9c7b",
										"font-weight": "normal"
									},
									"quantityInput": {
										"font-size": "14px",
										"padding-top": "15px",
										"padding-bottom": "15px"
									},
									"compareAt": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal",
										"color": "#7b9c7b",
										"font-size": "15px"
									}
								},
								"googleFonts": [
									"Karla",
									"Karla",
									"Karla",
									"Karla",
									"Karla",
									"Karla"
								]
							},
							"cart": {
								"contents": {
									"button": true
								},
								"styles": {
									"button": {
										"background-color": "#7b9c7b",
										"font-family": "Karla, sans-serif",
										"font-size": "14px",
										"padding-top": "15px",
										"padding-bottom": "15px",
										":hover": {
											"background-color": "#6f8c6f"
										},
										"font-weight": "normal",
										":focus": {
											"background-color": "#6f8c6f"
										}
									},
									"footer": {
										"background-color": "#ffffff"
									}
								},
								"googleFonts": [
									"Karla"
								]
							},
							"modalProduct": {
								"contents": {
									"img": false,
									"imgWithCarousel": true,
									"variantTitle": false,
									"buttonWithQuantity": true,
									"button": false,
									"quantity": false
								},
								"styles": {
									"product": {
										"@media (min-width: 601px)": {
											"max-width": "100%",
											"margin-left": "0px",
											"margin-bottom": "0px"
										}
									},
									"button": {
										"background-color": "#7b9c7b",
										"font-family": "Karla, sans-serif",
										"font-size": "14px",
										"padding-top": "15px",
										"padding-bottom": "15px",
										":hover": {
											"background-color": "#6f8c6f"
										},
										"font-weight": "normal",
										":focus": {
											"background-color": "#6f8c6f"
										}
									},
									"variantTitle": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"title": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"description": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"price": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									},
									"quantityInput": {
										"font-size": "14px",
										"padding-top": "15px",
										"padding-bottom": "15px"
									},
									"compareAt": {
										"font-family": "Karla, sans-serif",
										"font-weight": "normal"
									}
								},
								"googleFonts": [
									"Karla",
									"Karla",
									"Karla",
									"Karla",
									"Karla",
									"Karla"
								]
							},
							"toggle": {
								"styles": {
									"toggle": {
										"font-family": "Karla, sans-serif",
										"background-color": "#7b9c7b",
										":hover": {
											"background-color": "#6f8c6f"
										},
										"font-weight": "normal",
										":focus": {
											"background-color": "#6f8c6f"
										}
									},
									"count": {
										"font-size": "14px"
									}
								},
								"googleFonts": [
									"Karla"
								]
							},
							"option": {
								"styles": {
									"label": {
										"font-family": "Karla, sans-serif"
									},
									"select": {
										"font-family": "Karla, sans-serif"
									}
								},
								"googleFonts": [
									"Karla",
									"Karla"
								]
							},
							"productSet": {
								"styles": {
									"products": {
										"@media (min-width: 601px)": {
											"margin-left": "-20px"
										}
									}
								}
							}
						}
					});
				});
			});
		}
	};
});
